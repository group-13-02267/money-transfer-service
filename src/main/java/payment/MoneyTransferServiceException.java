package payment;

/**
 * Exception for when the Money Transfer fails
 */
public class MoneyTransferServiceException extends Exception {

    public MoneyTransferServiceException(String message) {
        super(message);
    }

    public MoneyTransferServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public MoneyTransferServiceException(Throwable cause) {
        super(cause);
    }

}
