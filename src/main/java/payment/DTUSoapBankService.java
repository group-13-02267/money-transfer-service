package payment;

import dtu.ws.fastmoney.*;
import responses.*;

import java.math.BigDecimal;

/**
 * Implementation of {@link BankService}
 * @author
 */
public class DTUSoapBankService implements BankService {

    /**
     * The generated soap bank service
     */
    private final dtu.ws.fastmoney.BankService bankService;

    /**
     * Instantiate a new Soap Bank Service
     */
    public DTUSoapBankService() {
        bankService = new BankServiceService().getBankServicePort();
    }

    @Override
    public DTUPayEmptyResponse transferMoneyFromTo(String debtor, String creditor, BigDecimal amount, String description) {
        try {
            bankService.transferMoneyFromTo(debtor, creditor, amount, description);
            return createSuccessResponse();
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }

    @Override
    public DTUPayResponse<Account> getAccountByCprNumber(String cpr) {
        try {
            return createSuccessResponse(bankService.getAccountByCprNumber(cpr));
        } catch (BankServiceException_Exception e) {
            return createErrorResponse(e.toString());
        }
    }

    /**
     * Helper for creating a successful response
     * @param response The successful response
     * @param <T> The response type
     * @return The response containing the response data
     */
    private <T> DTUPayResponse<T> createSuccessResponse(T response) {
        return new DTUPayResponseImpl<T>(response);
    }

    /**
     * Helper for creating an error response
     * @param errorMsg The error message
     * @param <T> The response type
     * @return The response containing the error message
     */
    private <T> DTUPayResponse<T> createErrorResponse(String errorMsg) {
        return new DTUPayResponseImpl<T>(new DTUPayErrorMessage(errorMsg));
    }

    /**
     * Helper for creating an empty successful response
     * @return The empty successful response
     */
    private DTUPayEmptyResponse createSuccessResponse() {
        return new DTUPayEmptyResponseImpl();
    }

    /**
     * Helper for creating an empty error response
     * @param errorMsg The error message
     * @return The empty error response with the error message
     */
    private DTUPayEmptyResponse creteErrorResponse(String errorMsg) {
        return new DTUPayEmptyResponseImpl(new DTUPayErrorMessage(errorMsg));
    }
}
