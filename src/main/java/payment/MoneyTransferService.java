package payment;

import DTO.PaymentDTO;

/**
 * Defines the methods a service for transfering money should have in DTUPay
 * @author
 */
public interface MoneyTransferService {

    /**
     * Transfer money
     * @param payment The payment containing the transfer information
     * @return True if the transfer was successful, false if not
     * @throws MoneyTransferServiceException Thrown if the transfer was unsuccessful
     */
    boolean transferMoney(PaymentDTO payment) throws MoneyTransferServiceException;
}
