package payment;

import DTO.PaymentDTO;
import dtu.ws.fastmoney.Account;
import responses.DTUPayEmptyResponse;
import responses.DTUPayResponse;

import java.math.BigDecimal;

/**
 * Implementation of {@link MoneyTransferService}
 * @author Mathias, David
 */
public class DTUMoneyTransferService implements MoneyTransferService {

    /**
     * The Bank Service
     */
    private final BankService bankService;

    public DTUMoneyTransferService(BankService bankService) {
        this.bankService = bankService;
    }

    /**
     * Handle an invalid Money Transfer response
     * @param message The error message
     * @throws MoneyTransferServiceException Always thrown
     */
    private static void handleInvalidResponse(String message) throws MoneyTransferServiceException {
        throw new MoneyTransferServiceException(message);
    }

    /**
     * Handle an invalid response
     * @param response The invalid response
     * @param message The error message
     * @throws MoneyTransferServiceException Thrown if the response is not null and the response did not succeed
     */
    private static void handleInvalidResponse(DTUPayEmptyResponse response, String message) throws MoneyTransferServiceException {
        if (response != null && !response.success()) {
            throw new MoneyTransferServiceException(message + ": " + response.getErrorMessage().getMessage());
        }
    }

    /**
     * Transfer money
     * @param payment The payment containing the transfer information
     * @return True if the transfer was successful, false if not
     * @throws MoneyTransferServiceException Thrown if the transfer was unsuccessful
     */
    public boolean transferMoney(PaymentDTO payment) throws MoneyTransferServiceException {

        DTUPayResponse<Account> senderAccountResponse = bankService.getAccountByCprNumber(payment.sender);
        DTUPayResponse<Account> receiverAccountResponse = bankService.getAccountByCprNumber(payment.receiver);

        if (!receiverAccountResponse.success() || !senderAccountResponse.success()) {
            return false;
        }

        String description = "Transferring " + payment.amount + " from " + payment.sender + " to " + payment.receiver;

        DTUPayEmptyResponse response = bankService.transferMoneyFromTo(
                senderAccountResponse.getResponse().getId(),
                receiverAccountResponse.getResponse().getId(),
                new BigDecimal(payment.amount),
                description
        );

        handleInvalidResponse(response, "Failed transferring money");

        return true;
    }
}
