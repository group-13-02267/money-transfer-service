package payment;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.AccountInfo;
import dtu.ws.fastmoney.User;
import responses.DTUPayEmptyResponse;
import responses.DTUPayResponse;

import java.math.BigDecimal;
import java.util.List;

/**
 * Bank service that all Bank service must implement
 * @author
 */
public interface BankService {

    /**
     * Transfer money from an account to another account
     * @param debtor The account to transfer from
     * @param creditor The account to transfer to
     * @param amount The amount to transfer
     * @param description The transfer description
     * @return Response containing the transfer status
     */
    DTUPayEmptyResponse transferMoneyFromTo(
            String debtor,
            String creditor,
            BigDecimal amount,
            String description);

    /**
     * Get the Account associated with a cpr
     * @param cpr The cpr that the account is associated with
     * @return The account associated with the cpr
     */
    DTUPayResponse<Account> getAccountByCprNumber(
            String cpr);
}
