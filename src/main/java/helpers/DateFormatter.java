package helpers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;

import static java.time.temporal.ChronoField.*;

/**
 * Helper functions for working with LocalDate
 */
public class DateFormatter {

    /**
     * Formatter representing dd-mm-yyyy
     */
    private static DateTimeFormatter formatter = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendValue(DAY_OF_MONTH, 2)
            .appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2)
            .appendLiteral('-')
            .appendValue(YEAR, 4)
            .toFormatter();

    /**
     * Convert a string to a LocaLDate object
     * @param date A string date with format dd-mm-yyyy
     * @return LocalDate representing the provided {@code date}
     */
    public static LocalDate convertStringDate(String date) {
        try {
            return LocalDate.parse(date, formatter);
        } catch (DateTimeParseException e) {
            return null;
        }
    }

    /**
     * Convert a LocalDate to a string
     * @param date The LocalDate to convert to a string
     * @return A string representation of the LocalDate with format dd-mm-yyyy
     */
    public static String convertDateToString(LocalDate date) {
        return date.format(formatter);
    }

}
