package messaging;

import core.AsyncSender;
import core.RabbitSender;

/**
 * Message Sender for sending messages using RabbitMQ
 *
 * @author Mathias
 */
public class RabbitMqMessageSenderService implements MessageSender {
    @Override
    public void sendMessage(Object content, String route) {
        AsyncSender<Object> eventSender = new RabbitSender<>("payment", route);
        eventSender.send(content);
    }
}
