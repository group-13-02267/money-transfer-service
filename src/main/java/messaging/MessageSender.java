package messaging;

/**
 * Interface that all message senders must implement
 *
 * @author Mathias
 */
public interface MessageSender {
    /**
     * Send a message
     *
     * @param content The message payload
     * @param route   The route to send the message
     */
    void sendMessage(Object content, String route);
}
