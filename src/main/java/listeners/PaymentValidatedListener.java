package listeners;

import com.google.gson.Gson;
import DTO.PaymentDTO;
import core.AsyncService;
import messaging.MessageSender;
import payment.MoneyTransferService;
import payment.MoneyTransferServiceException;

/**
 * Payment Validation Listener.
 * Receives payment requests when a transaction has been validated.
 * @author George
 */
public class PaymentValidatedListener extends AsyncService<PaymentDTO> {

    /**
     * The exchange to listen on
     */
    private final static String exchange = "payment";

    /**
     * The message sender
     */
    private final MessageSender sender;

    /**
     * The money transfer service
     */
    private final MoneyTransferService transferService;

    /**
     * Instantiate a new PaymentValidatedListener
     * @param sender The message sender
     */
    public PaymentValidatedListener(MoneyTransferService transferService, MessageSender sender) {
        super(exchange, "transaction.payment.validated.*");
        this.sender = sender;
        this.transferService = transferService;
    }

    @Override
    protected PaymentDTO handleMessage(String message) {
        return new Gson().fromJson(message, PaymentDTO.class);
    }

    @Override
    public void receive(PaymentDTO payment) {
        boolean paymentStatus = false;

        try {
            paymentStatus = transferService.transferMoney(payment);
        } catch (MoneyTransferServiceException ignored) {}

        payment.status = paymentStatus;

        sender.sendMessage(payment, "transaction.payment.handled." + payment.id);

        System.out.println("payment: " + payment.status);
    }
}
