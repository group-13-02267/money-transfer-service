package listeners;

import DTO.PaymentDTO;
import com.google.gson.Gson;
import core.AsyncService;
import messaging.MessageSender;
import payment.MoneyTransferService;
import payment.MoneyTransferServiceException;

/**
 * Refund Validated Listener.
 * Receives events when a refund has been validated.
 * @author George
 */
public class RefundValidatedListener extends AsyncService<PaymentDTO> {

    /**
     * The exchange to listen on
     */
    private static final String exchange = "payment";

    /**
     * The message sender
     */
    private final MessageSender sender;

    /**
     * The money transfer service
     */
    private final MoneyTransferService transferService;
    /**
     * Instantiate a new RefundValidatedListener
     * @param sender The message sender
     */
    public RefundValidatedListener(MoneyTransferService transferService, MessageSender sender) {
        super(exchange, "transaction.refund.validated.*");
        this.transferService = transferService;
        this.sender = sender;
    }

    @Override
    protected PaymentDTO handleMessage(String s) {
        return new Gson().fromJson(s, PaymentDTO.class);
    }

    @Override
    public void receive(PaymentDTO paymentDTO) {

        boolean paymentStatus = false;

        try {
            paymentStatus = transferService.transferMoney(paymentDTO);
        } catch (MoneyTransferServiceException ignored) {
        }

        paymentDTO.status = paymentStatus;

        sender.sendMessage(paymentDTO, "transaction.refund.handled." + paymentDTO.id);

        System.out.println("refund: " + paymentDTO.status);
    }
}
