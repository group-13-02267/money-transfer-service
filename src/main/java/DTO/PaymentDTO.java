package DTO;

import helpers.DateFormatter;

import java.time.LocalDate;

/**
 * Represents a payment as a DTO
 * @author Georg
 */
public class PaymentDTO {

    /**
     * The validation status
     */
    public boolean status;

    /**
     * The unique payment request id
     */
    public String id;

    /**
     * The sender id
     */
    public String sender;

    /**
     * The payment token
     */
    public String token;

    /**
     * The receiver id
     */
    public String receiver;

    /**
     * The payment amount
     */
    public float amount;

    /**
     * Representing if the payment is a refund or not
     */
    public boolean isRefund;

    /**
     * The payment creation date
     */
    public String createdDate;

    /**
     * Instantiate a new PaymentDTO
     * @param id The unique request id
     * @param token The payment token
     * @param sender the sender id
     * @param receiver The receiver id
     * @param amount The payment amount
     * @param status The payment validation status
     * @param isRefund Is the payment a refund or a payment
     */
    public PaymentDTO(String id, String token, String sender, String receiver, float amount, boolean status, boolean isRefund) {
        this.id = id;
        this.token = token;
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.status = status;
        this.isRefund = isRefund;
        createdDate = DateFormatter.convertDateToString(LocalDate.now());
    }
}
