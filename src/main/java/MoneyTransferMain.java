import DTO.PaymentDTO;
import core.AsyncException;
import core.AsyncService;
import core.Microservice;
import listeners.PaymentValidatedListener;
import listeners.RefundValidatedListener;
import messaging.MessageSender;
import messaging.RabbitMqMessageSenderService;
import payment.DTUMoneyTransferService;
import payment.DTUSoapBankService;
import payment.MoneyTransferService;

/**
 * Start service and set up listeners
 * @author George
 */
public class MoneyTransferMain extends Microservice {

    public static void main(String[] args) throws Exception {
        new MoneyTransferMain().startup();
    }

    @Override
    public void startup() throws AsyncException {
        MessageSender sender = new RabbitMqMessageSenderService();
        MoneyTransferService transferService = new DTUMoneyTransferService(new DTUSoapBankService());
        AsyncService<PaymentDTO> paymentListener = new PaymentValidatedListener(transferService, sender);
        AsyncService<PaymentDTO> refundListener = new RefundValidatedListener(transferService, sender);
        paymentListener.listen();
        refundListener.listen();
    }
}
