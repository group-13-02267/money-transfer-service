package responses;

/**
 * Response that holds data
 *
 * @param <T> The data type
 * @author Mathias
 */
public class DTUPayResponseImpl<T> extends DTUPayEmptyResponseImpl implements DTUPayResponse<T> {

    /**
     * The response data
     */
    private T response;

    /**
     * The response error meessage
     */
    private ErrorMessage errorMessage;

    /**
     * Instantiate a successful response
     *
     * @param response The response data
     */
    public DTUPayResponseImpl(T response) {
        this.response = response;
    }

    /**
     * Instantiate an unsuccessful response
     *
     * @param errorMessage The error message
     */
    public DTUPayResponseImpl(ErrorMessage errorMessage) {
        super(errorMessage);
    }

    @Override
    public T getResponse() {
        return response;
    }
}
