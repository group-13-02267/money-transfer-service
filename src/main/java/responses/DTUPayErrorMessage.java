package responses;

/**
 * Error Message
 *
 * @author Mathias
 */
public class DTUPayErrorMessage implements ErrorMessage {

    /**
     * The error message
     */
    private String message;

    /**
     * Instantiate a new Error Message
     *
     * @param message The error message
     */
    public DTUPayErrorMessage(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
