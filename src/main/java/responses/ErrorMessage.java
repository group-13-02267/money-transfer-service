package responses;

/**
 * Error message
 */
public interface ErrorMessage {
    /**
     * Get the error message
     *
     * @return The error message
     */
    String getMessage();
}
