package responses;

/**
 * Empty response implementation
 *
 * @author Mathias
 */
public class DTUPayEmptyResponseImpl implements DTUPayEmptyResponse {

    /**
     * The error message if present
     */
    private ErrorMessage errorMessage;

    /**
     * Instantiate a successful empty response
     */
    public DTUPayEmptyResponseImpl() {
    }

    /**
     * Instantiate a unsuccessful empty response with an error message
     *
     * @param errorMessage The error message to associate with the unsuccessful response
     */
    public DTUPayEmptyResponseImpl(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean success() {
        return errorMessage == null;
    }

    @Override
    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }
}
