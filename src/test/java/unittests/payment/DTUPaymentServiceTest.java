package unittests.payment;

import DTO.PaymentDTO;
import dtu.ws.fastmoney.Account;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import payment.BankService;
import payment.DTUMoneyTransferService;
import responses.DTUPayResponse;
import responses.DTUPayResponseImpl;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

/**
 * unittest of {@link DTUMoneyTransferService}
 * @author Nick, Georg
 */
public class DTUPaymentServiceTest {

    private final String customerId = "user-id";
    private BankService bankService;
    private DTUMoneyTransferService paymentService;

    @Before
    public void setup() {
        bankService = mock(BankService.class);
        paymentService = new DTUMoneyTransferService(bankService);
    }

    @Test
    public void shouldValidatePayment() throws Exception {
        String merchant = "merchant";
        float amount = 100;

        DTUPayResponse<Account> customerDTUAccountResponse = new DTUPayResponseImpl<>(new Account() {{
            id = customerId;
        }});

        DTUPayResponse<Account> merchantDTOAccountResponse = new DTUPayResponseImpl<>(new Account() {{
            id = merchant;
        }});

        when(bankService.getAccountByCprNumber(customerId)).thenReturn(customerDTUAccountResponse);
        when(bankService.getAccountByCprNumber(merchant)).thenReturn(merchantDTOAccountResponse);

        PaymentDTO payment = new PaymentDTO("transaction-id", "token", customerId, merchant, amount, false, false);

        Assert.assertTrue(paymentService.transferMoney(payment));
        verify(bankService).transferMoneyFromTo(eq(customerId), eq(merchant), eq(new BigDecimal(amount)), anyString());
    }
}
