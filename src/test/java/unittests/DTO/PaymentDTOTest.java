package unittests.DTO;

import DTO.PaymentDTO;
import helpers.DateFormatter;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

/**
 * unittest of {@link PaymentDTO}
 * @author Georg
 */
public class PaymentDTOTest {

    PaymentDTO paymentDTO;

    @Before
    public void setUp()
    {
        paymentDTO = new PaymentDTO(
                "valid-id",
                "valid-token-value",
                "sender",
                "receiver",
                100,
                false,
                false
        );
    }

    @Test
    public void it_holds_an_id()
    {
        assertEquals("valid-id", paymentDTO.id);
    }

    @Test
    public void it_holds_a_token_value()
    {
        assertEquals("valid-token-value", paymentDTO.token);
    }

    @Test
    public void it_holds_the_sender_of_money()
    {
        assertEquals("sender", paymentDTO.sender);
    }

    @Test
    public void it_holds_the_receiver_of_money()
    {
        assertEquals("receiver", paymentDTO.receiver);
    }

    @Test
    public void it_holds_the_amount_of_money()
    {
        assertEquals(100, paymentDTO.amount, 0);
    }

    @Test
    public void it_holds_the_status_of_the_payment()
    {
        assertEquals(false, paymentDTO.status);
    }

    @Test
    public void it_holds_an_isRefund_flag()
    {
        assertEquals(false, paymentDTO.isRefund);
    }

    @Test
    public void it_holds_a_date_of_creation()
    {
        assertEquals(DateFormatter.convertDateToString(LocalDate.now()), paymentDTO.createdDate);
    }
}
