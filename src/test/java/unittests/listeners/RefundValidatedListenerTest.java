package unittests.listeners;

import DTO.PaymentDTO;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import listeners.RefundValidatedListener;
import messaging.MessageSender;
import messaging.RabbitMqMessageSenderService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import payment.BankService;
import payment.DTUMoneyTransferService;
import payment.DTUSoapBankService;
import payment.MoneyTransferService;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * unittest for {@link RefundValidatedListener}
 * @author Georg
 */
public class RefundValidatedListenerTest {
    BankServiceService bankService = new BankServiceService();
    String account1;
    String account2;

    @Before
    public void create_users() throws BankServiceException_Exception {
        User userA = new User();
        userA.setCprNumber("13UserA");
        userA.setFirstName("John");
        userA.setLastName("Doe");

        User userB = new User();
        userB.setCprNumber("13UserB");
        userB.setFirstName("Jane");
        userB.setLastName("Doe");


        account1 = bankService.getBankServicePort().createAccountWithBalance(userA, new BigDecimal(500));
        account2 = bankService.getBankServicePort().createAccountWithBalance(userB, new BigDecimal(500));
    }

    @After
    public void delete_users() throws BankServiceException_Exception {
        bankService.getBankServicePort().retireAccount(account1);
        bankService.getBankServicePort().retireAccount(account2);
    }

    @Test
    public void it_updates_the_status_to_true_if_refund_succeeds()
    {
        MessageSender sender = mock(RabbitMqMessageSenderService.class);
        BankService bankService = new DTUSoapBankService();
        MoneyTransferService transferService = new DTUMoneyTransferService(bankService);
        RefundValidatedListener listener = new RefundValidatedListener(transferService, sender);

        PaymentDTO paymentDTO = new PaymentDTO(
                "id",
                "token",
                "13UserB",
                "13UserA",
                100,
                false,
                true
        );

        listener.receive(paymentDTO);

        assertTrue(paymentDTO.status);
    }

    @Test
    public void if_payment_fails_status_keeps_being_false()
    {
        MessageSender sender = mock(RabbitMqMessageSenderService.class);
        BankService bankService = new DTUSoapBankService();
        MoneyTransferService transferService = new DTUMoneyTransferService(bankService);
        RefundValidatedListener listener = new RefundValidatedListener(transferService, sender);

        PaymentDTO paymentDTO = new PaymentDTO(
                "id",
                "token",
                "13UserB",
                "13UserA",
                600,
                false,
                true
        );

        listener.receive(paymentDTO);

        assertFalse(paymentDTO.status);
    }

    @Test
    public void it_pushes_a_message_with_the_updated_paymentDTO()
    {
        MessageSender sender = mock(RabbitMqMessageSenderService.class);
        BankService bankService = new DTUSoapBankService();
        MoneyTransferService transferService = new DTUMoneyTransferService(bankService);
        RefundValidatedListener listener = new RefundValidatedListener(transferService, sender);

        PaymentDTO paymentDTO = new PaymentDTO(
                "id",
                "token",
                "13UserA",
                "13UserB",
                100,
                false,
                true
        );

        listener.receive(paymentDTO);

        verify(sender, times(1)).sendMessage(paymentDTO, "transaction.refund.handled." + paymentDTO.id);
        assertTrue(paymentDTO.status);
    }
}
