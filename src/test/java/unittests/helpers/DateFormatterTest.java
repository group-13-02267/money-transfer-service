package unittests.helpers;

import helpers.DateFormatter;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Test of {@link DateFormatter}
 * @author Georg
 */
public class DateFormatterTest {

    @Test
    public void it_formats_the_date_to_the_desired_format_from_LocalDate() {
        LocalDate date = LocalDate.of(2020, 1, 1);

        String formattedDate = DateFormatter.convertDateToString(date);

        assertEquals("01-01-2020", formattedDate);
    }

    @Test
    public void it_can_create_a_localDate_from_a_String() {
        String date = "01-01-2020";

        LocalDate localDate = DateFormatter.convertStringDate(date);

        assertEquals("2020-01-01", localDate.toString());
    }

    @Test
    public void it_returns_null_when_wrong_string_given() {
        String date = "not-a-date";

        LocalDate localDate = DateFormatter.convertStringDate(date);

        assertNull(localDate);
    }
}
