package servicetests;

import DTO.PaymentDTO;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import listeners.PaymentValidatedListener;
import messaging.MessageSender;
import payment.DTUMoneyTransferService;
import payment.MoneyTransferService;
import payment.MoneyTransferServiceException;

import static org.mockito.Mockito.*;

/**
 * @author Georg
 */
public class MoneyTransferSteps {

    MessageSender sender = mock(MessageSender.class);
    MoneyTransferService transferService = mock(DTUMoneyTransferService.class);
    PaymentValidatedListener listener = new PaymentValidatedListener(transferService, sender);

    PaymentDTO payment;

    @When("the service receives a {string} event")
    public void theServiceReceivesAEvent(String arg0) throws MoneyTransferServiceException {
        payment = new PaymentDTO(
                "id",
                "token",
                "sender",
                "receiver",
                100,
                false,
                false
        );

        when(transferService.transferMoney(any())).thenReturn(true);
        listener.receive(payment);
    }

    @Then("it initiates a money transfer with the bank for the payment")
    public void itInitiatesAMoneyTransferWithTheBankForThePayment() throws MoneyTransferServiceException {
        verify(transferService).transferMoney(payment);
    }

    @And("the {string} event is broadcast")
    public void theEventIsBroadcast(String arg0) {
        verify(sender).sendMessage(payment, "transaction.payment.handled." + payment.id);
    }
}
