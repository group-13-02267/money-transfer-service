Feature: Money transfer feature
  Scenario: Successful Payment Money Transfer
    When the service receives a "transaction.payment.validated" event
    Then it initiates a money transfer with the bank for the payment
    And the "transaction.payment.handled" event is broadcast
